# Albathanext test app

## How to start application:

Start `docker-compose up` from backend project before running `docker-compose up` from here.

### DEV mode

1. Run `docker-compose up`
2. After steps above, you can open http://localhost:4000
3. Also, you can install node_modules for IDE (start commands from project root):
    ```shell
    npm install
    cd packages/frontend
    npm install
    cd ../shared
    npm install
    ```

### PROD mode

1. Run `docker-compose -f docker-compose.prod.yaml up`
2. After steps above, you can open http://localhost:4000
