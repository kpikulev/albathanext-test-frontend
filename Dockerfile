FROM node:17.8.0-slim AS build
COPY .npmrc /root/.npmrc
WORKDIR /app
COPY .eslintignore /app
COPY .prettierignore /app
COPY *.json /app/
COPY packages/shared/*.json /app/packages/shared/
COPY packages/frontend/*.json /app/packages/frontend/
RUN npm ci && \
  npm ci --prefix=packages/shared && \
  npm ci --prefix=packages/frontend && \
  wait
COPY packages/shared /app/packages/shared
COPY packages/frontend /app/packages/frontend
ARG frontend_backend_public_url=http://localhost:3000
ARG frontend_backend_private_url=http://backend:3000
ARG app_version
ARG app_source_branch
ARG app_source_commit
ARG app_build_time
ENV BACKEND_PUBLIC_URL=$frontend_backend_public_url \
    BACKEND_PRIVATE_URL=$frontend_backend_private_url \
    APP_VERSION=$app_version \
    APP_SOURCE_BRANCH=$app_source_branch \
    APP_SOURCE_COMMIT=$app_source_commit \
    APP_BUILD_TIME=$app_build_time
RUN npm run build
RUN npm run lint
RUN (cd /app/packages/shared; npm prune --production)

FROM node:17.8.0-slim AS frontend
ARG app_version
ARG app_source_branch
ARG app_source_commit
ARG app_build_time
COPY --from=build /app/packages/frontend/node_modules /app/node_modules
COPY --from=build /app/packages/shared /app/node_modules/@template/shared
COPY --from=build /app/packages/frontend/.next /app/.next
COPY --from=build /app/packages/frontend/*.json /app/
COPY --from=build /app/packages/frontend/next.config.js /app/
ENV NODE_ENV=production \
    APP_VERSION=$app_version \
    APP_SOURCE_BRANCH=$app_source_branch \
    APP_SOURCE_COMMIT=$app_source_commit \
    APP_BUILD_TIME=$app_build_time
CMD cd app; npm run start


FROM nginx:1.17.6-alpine AS nginx
COPY packages/frontend/nginx/default.conf /etc/nginx/conf.d/
