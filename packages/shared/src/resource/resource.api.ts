import { Response } from "express";

import { APIRoutes } from "../api-routes";
import { ApiErrors } from "../api-types";
import { MessagesConstraints } from "../utils/message-formatter";

import { UpdateResourceDto } from "./dto/update-resource-dto";
import { CreateResourceDto } from "./dto/create-resource-dto";

export enum ResourceType {
  IMAGE = "image"
}

export class ResourceDraft {
  originalFileName: string;

  originalFileExtension: string;

  resourceName: string;

  resourceExtension: string;

  resourceType: ResourceType;

  resourceData: string;
}

export class Resource extends ResourceDraft {
  resourceId: string;
}

export declare type ResourceDeleteResponse = Pick<Resource, "resourceId">;

export interface ResourceApi {
  createResource(createResourceDto: CreateResourceDto, ...omit: any[]): unknown;
  getResource(id: string, ...omit: any[]): unknown;
  getResources(...omit: any[]): unknown;

  updateResource(id: string, updateResourceDto: UpdateResourceDto, ...omit: any[]): unknown;

  deleteResource(id: string, ...omit: any[]): unknown;
}

export type ResponseBody<T> = {
  payload?: T;
  messages?: Partial<MessagesConstraints>;
  error?: ApiErrors;
};

type Modify<T, R extends T> = Omit<T, keyof R> & R;

export type ResourceFrontendAPI = Pick<Modify<ResourceApi, ResourceFrontendAPIStructure>, keyof ResourceApi>;
interface ResourceFrontendAPIStructure {
  createResource(createResourceDto: CreateResourceDto, ...omit: any[]): Promise<ResponseBody<Resource>>;

  getResource(id: string, ...omit: any[]): Promise<ResponseBody<Resource>>;

  getResources(...omit: any[]): Promise<Resource[]>;

  updateResource(id: string, updateResourceDto: UpdateResourceDto, ...omit: any[]): Promise<ResponseBody<Resource>>;

  deleteResource(id: string, ...omit: any[]): Promise<ResourceDeleteResponse>;
}

export type ResourceBackendAPI = Pick<Modify<ResourceApi, ResourceBackendAPIStructure>, keyof ResourceApi>;
interface ResourceBackendAPIStructure {
  createResource(createResourceDto: CreateResourceDto, ...omit: any[]): Promise<Response<ResponseBody<Resource>>>;

  getResource(id: string, ...omit: any[]): Promise<Resource>;

  getResources(...omit: any[]): Promise<Resource[]>;

  updateResource(
    id: string,
    updateResourceDto: UpdateResourceDto,
    ...omit: any[]
  ): Promise<Response<ResponseBody<Resource>>>;

  deleteResource(id: string, ...omit: any[]): Promise<ResourceDeleteResponse>;
}

export const RESOURCE_ROUTES: APIRoutes<ResourceApi> = {
  createResource: () => "/resources",
  getResource: (id: string) => `/resources/${id}`,
  getResources: () => "/resources",
  updateResource: (id: string) => `/resources/${id}`,
  deleteResource: (id: string) => `/resources/${id}`
};
