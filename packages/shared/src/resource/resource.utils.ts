import { Resource, ResourceDraft } from "./resource.api";

export const makeResourceDraftFromResource: (resource: Resource) => ResourceDraft = resource => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { resourceId, ...rest }: Resource = resource;
  return rest;
};
