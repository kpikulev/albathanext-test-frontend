import { IsDefined, ValidateNested } from "class-validator";
import { Type } from "class-transformer";

import { ResourceDraft } from "../resource.api";

export class CreateResourceDto {
  @IsDefined()
  @ValidateNested()
  @Type(() => ResourceDraft)
  resource: ResourceDraft;
}
