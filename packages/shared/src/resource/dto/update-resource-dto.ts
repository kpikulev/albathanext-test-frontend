import { IsDefined, ValidateNested } from "class-validator";
import { Type } from "class-transformer";

import { ResourceDraft } from "../resource.api";

export class UpdateResourceDto {
  @IsDefined()
  // @IsNotEmptyObject()
  // @IsObject()
  @ValidateNested()
  @Type(() => ResourceDraft)
  resource: ResourceDraft;
}
