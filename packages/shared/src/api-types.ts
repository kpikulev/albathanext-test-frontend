export type ApiError = { status: number; description: { [key: string]: string } };

export enum ApiErrors {
  placeIsAgreed = "place is agreed",
  addToAsanaError = "add to asana error",
  syncWithAsanaError = "sync with asana error"
}

export type ApiResponse<T> = { payload: T | undefined; error: ApiError | undefined };
