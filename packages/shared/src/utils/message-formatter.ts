import { ValidationError } from "class-validator";

import { MovieDraft } from "../movie/movie.api";

function dfs(src: ValidationError[], dest: Partial<MessagesConstraints>): void {
  for (const validationError of src) {
    const property: keyof MessagesConstraints = validationError.property as keyof MessagesConstraints;
    dest[property] = validationError.children?.length ? {} : validationError.constraints;

    if (validationError.children?.length) {
      dfs(validationError.children, dest[property] as unknown as Partial<MessagesConstraints>);
    }
  }
}

export interface MessagesConstraints {
  movie: { [key in keyof Partial<MovieDraft>]: { [validationConstraint: string]: string } };
}

export class MessageFormatter {
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types,@typescript-eslint/explicit-function-return-type
  public static format(validationErrors: ValidationError[]) {
    const dest: Partial<MessagesConstraints> = {};
    dfs(validationErrors, dest);
    return dest;
  }
}
