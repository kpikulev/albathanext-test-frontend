import { APIRoutes } from "../api-routes";

import { UserInfo } from "./user-info";

export interface UserApi {
  me(...omitted: any): Promise<UserInfo>;
}

export const PREFIX: string = "/auth";

export const LOGIN_ROUTE: string = `${PREFIX}/login`;
export const LOGIN_REDIRECT_PARAM: string = "redirect";

export const LOGOUT_ROUTE: string = `${PREFIX}/logout`;

export const AUTH_ROUTES: APIRoutes<UserApi> = {
  me: () => `${PREFIX}/me`
};
