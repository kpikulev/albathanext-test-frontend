export const roleTypes: readonly ["admin", "chairman"] = ["admin", "chairman"] as const;

export type RoleType = typeof roleTypes[number];

export type UserRoleAdmin = { type: "admin" };

export type UserRoleHead = {
  type: "chairman";
  projectId: string;
};

export type RoleDataBase = {
  name?: string;
};

export type RoleData = UserRoleAdmin | UserRoleHead;

export interface UserInfo {
  id: string | null;

  name: string;

  email: string;

  roles: RoleData[];
}
