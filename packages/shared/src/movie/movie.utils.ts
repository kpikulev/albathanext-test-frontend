import { Movie, MovieDraft } from "./movie.api";

export const makeMovieDraftFromMovie: (movie: Movie) => MovieDraft = movie => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { id, ...rest }: Movie = movie;
  return rest;
};
