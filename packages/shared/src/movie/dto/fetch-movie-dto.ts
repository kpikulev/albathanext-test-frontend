import { IsDefined } from "class-validator";

export class FetchMovieDto {
  @IsDefined()
  movieId: number;
}
