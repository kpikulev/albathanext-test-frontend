import { IsNotEmpty } from "class-validator";

import { APIRoutes } from "../api-routes";
import { ApiErrors } from "../api-types";
import { MessagesConstraints } from "../utils/message-formatter";

export class MovieDraft {
  @IsNotEmpty({ message: "Original title should be not empty" })
  original_title: string;

  @IsNotEmpty({ message: "Poster path should be not empty" })
  poster_path: string;

  @IsNotEmpty({ message: "Tagline should be not empty" })
  tagline: string;

  @IsNotEmpty({ message: "Release date should be not empty" })
  release_date: string;

  @IsNotEmpty({ message: "Overview should be not empty" })
  overview: string;

  @IsNotEmpty({ message: "Budget should be not empty" })
  budget: number;

  @IsNotEmpty({ message: "Homepage should be not empty" })
  homepage: string;

  @IsNotEmpty({ message: "Revenue should be not empty" })
  revenue: number;

  @IsNotEmpty({ message: "Vote average should be not empty" })
  vote_average: number;
}

export class Movie extends MovieDraft {
  id: number;
}

export declare type MovieDeleteResponse = Pick<Movie, "id">;

export interface MovieApi {
  getMovie(id: number, ...omit: any[]): unknown;
  getMovies(...omit: any[]): unknown;
}

export interface MoviesResponse {
  page: number;
  results: Movie[];
  total_pages: number;
  total_results: number;
}

export interface MovieResponse {
  movie: Movie;
}

export type ResponseBody<T> = {
  payload?: T;
  messages?: Partial<MessagesConstraints>;
  error?: ApiErrors;
};

type Modify<T, R extends T> = Omit<T, keyof R> & R;

export type MovieFrontendAPI = Pick<Modify<MovieApi, MovieFrontendAPIStructure>, keyof MovieApi>;
interface MovieFrontendAPIStructure {
  getMovie(id: number, ...omit: any[]): Promise<MovieResponse>;
  getMovies(...omit: any[]): Promise<MoviesResponse>;
}

export const MOVIE_ROUTES: APIRoutes<MovieApi> = {
  getMovie: (id: string) => `/movies/${id}`,
  getMovies: () => "/movies"
};
