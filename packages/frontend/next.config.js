const regexEqual = (x, y) => {
  return (
    x instanceof RegExp &&
    y instanceof RegExp &&
    x.source === y.source &&
    x.global === y.global &&
    x.ignoreCase === y.ignoreCase &&
    x.multiline === y.multiline
  )
}

module.exports = {
  webpack: function (config) {
    const oneOf = config.module.rules.find((rule) => typeof rule.oneOf === 'object');
    if (oneOf) {
      const moduleSassRule = oneOf.oneOf.find((rule) =>
        regexEqual(rule.test, /\.module\.(scss|sass)$/)
      )
      const newUse = [];
      let index = 0;
      for (const useItem of moduleSassRule.use) {
        if (index === 1) {
          newUse.push({
            loader: '@teamsupercell/typings-for-css-modules-loader',
          });
        }
        newUse.push(useItem);
        index++;
      }
      moduleSassRule.use = newUse;
    }

    return config;
  },
  images: {
    domains: ['image.tmdb.org'],
  },
  eslint: {
    ignoreDuringBuilds: true,
  },
  env: {
    BACKEND_PUBLIC_URL: process.env.BACKEND_PUBLIC_URL,
  },
};
