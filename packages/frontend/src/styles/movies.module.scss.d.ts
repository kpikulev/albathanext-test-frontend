declare namespace MoviesModuleScssNamespace {
  export interface IMoviesModuleScss {
    moviesPage: string;
    moviesPageBg: string;
  }
}

declare const MoviesModuleScssModule: MoviesModuleScssNamespace.IMoviesModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: MoviesModuleScssNamespace.IMoviesModuleScss;
};

export = MoviesModuleScssModule;
