import { AppProps } from "next/app";
import { NextPage } from "next";
import React from "react";

import { wrapper } from "~/store";

import "~/styles/global.scss";

const MyApp: NextPage<AppProps> = ({ Component, pageProps }: AppProps) => {
  return <Component {...pageProps} />;
};

export default wrapper.withRedux(MyApp);
