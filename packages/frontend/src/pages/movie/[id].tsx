import React from "react";
import { GetServerSideProps, GetServerSidePropsResult, NextPage } from "next";
import { END } from "redux-saga";

import styles from "~/styles/movies.module.scss";
import { Page } from "~/components/Page";
import { wrapper } from "~/store";
import { Header } from "~/components/Header";
import { fetchMovie } from "~/components/Movie/store/actions";
import { MovieComponent } from "~/components/Movie/Movie";

const Index: NextPage = () => {
  return (
    <div className={styles.moviesPage}>
      <Page>
        <div className={styles.moviesPageBg} />
        <Header />
        <MovieComponent />
      </Page>
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = wrapper.getServerSideProps(store => async (context): Promise<
  GetServerSidePropsResult<{ a: "d" }>
> => {
  const movieId: number = Number(context.query.id);

  store.dispatch(fetchMovie({ movieId }));

  store.dispatch(END);
  return await store.sagaTask.toPromise();
});

export default Index;
