import React from "react";
import { GetServerSideProps, GetServerSidePropsResult, NextPage } from "next";
import { END } from "redux-saga";

import { Header } from "~/components/Header";
import styles from "~/styles/index.module.scss";
import { Page } from "~/components/Page";
import { wrapper } from "~/store";

const Index: NextPage = () => {
  return (
    <div className={styles.indexPage}>
      <Page>
        <div className={styles.indexPageBg} />
        <div className={styles.indexPageBg2} />
        <Header />
        <h1>Albathanext test app</h1>
        <p>Here, I want to show you my practice of using Spring Boot + Next.js.</p>
        <p>I hope, you have just easily started this application using Docker :)</p>
      </Page>
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = wrapper.getServerSideProps(store => async (): Promise<
  GetServerSidePropsResult<{ a: "d" }>
> => {
  store.dispatch(END);
  return await store.sagaTask.toPromise();
});

export default Index;
