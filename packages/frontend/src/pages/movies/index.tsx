import React from "react";
import { GetServerSideProps, GetServerSidePropsResult, NextPage } from "next";
import { END } from "redux-saga";

import styles from "~/styles/movies.module.scss";
import { Page } from "~/components/Page";
import { wrapper } from "~/store";
import { Header } from "~/components/Header";
import { Movies } from "~/components/Movie/Movies";
import { fetchMovies } from "~/components/Movie/store/actions";

const Index: NextPage = () => {
  return (
    <div className={styles.moviesPage}>
      <Page>
        <div className={styles.moviesPageBg} />
        <Header />
        <Movies />
      </Page>
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = wrapper.getServerSideProps(store => async (): Promise<
  GetServerSidePropsResult<{ a: "d" }>
> => {
  store.dispatch(fetchMovies());
  store.dispatch(END);
  return await store.sagaTask.toPromise();
});

export default Index;
