import { ApiError } from "@template/shared/api-types";

type ResponseMapper<T> = (data: Response) => Promise<T> | T;

export class FetchError<T> extends Error {
  constructor(message: string, readonly status: number, private readonly response: T) {
    super(message);
    this.name = "FetchError";
    Object.setPrototypeOf(this, FetchError.prototype);
  }

  get responseData(): T {
    return this.response;
  }
}

function handleResponse<R>(response: Response, responseMapper?: ResponseMapper<R>): Promise<R> {
  if (responseMapper) {
    return Promise.resolve(responseMapper(response));
  } else {
    const contentType: string | null = response.headers.get("content-type");
    if (contentType?.includes("application/json")) {
      return Promise.resolve<R>(response.json());
    } else {
      throw new Error("Default handler supports only json data");
    }
  }
}

export const BASE_URL: string =
  typeof window === "undefined" ? process.env.BACKEND_PRIVATE_URL ?? "" : process.env.BACKEND_PUBLIC_URL ?? "";

export interface FetchOptions extends RequestInit {
  body?: any;
  queryParams?: Record<string, any>;
}

export async function apiFetch<T>(
  url: string,
  options: FetchOptions = {},
  responseMapper?: ResponseMapper<T>,
  autoContentType?: boolean
): Promise<T> {
  const apiUrl: URL = new URL(`${BASE_URL}${url}`);
  if (options?.queryParams) {
    Object.keys(options.queryParams).forEach(key => {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment,@typescript-eslint/typedef
      const value = options?.queryParams?.[key];
      const isValueValid: boolean = value !== null && value !== undefined && value !== "";
      isValueValid && apiUrl.searchParams.append(key, String(options?.queryParams?.[key]));
    });
  }
  const response: Response = await fetch(apiUrl.toString(), {
    ...options,
    headers: {
      Accept: "application/json",
      ...(autoContentType ? {} : { "Content-Type": "application/json" }),
      ...options?.headers
    },
    credentials: "include"
  });

  if (!response.ok) {
    throw new FetchError<{ error: ApiError }>(
      response.statusText,
      response.status,
      response.headers?.get("content-type")?.includes("application/json") ? await response.json() : response.text()
    );
  }

  return handleResponse(response, responseMapper);
}
