import { applyMiddleware, combineReducers, createStore, Middleware, Store, StoreEnhancer } from "redux";
import { createWrapper, MakeStore } from "next-redux-wrapper";
import createSagaMiddleware, { SagaMiddleware } from "redux-saga";

import rootSaga from "~/saga";
import { moviesReducer, MoviesStore } from "~/components/Movie/store";

type BindMiddleware = (middleware: Middleware[]) => StoreEnhancer;
const bindMiddleware: BindMiddleware = (middleware: Middleware[]): StoreEnhancer => {
  if (process.env.NODE_ENV !== "production") {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment,@typescript-eslint/no-require-imports,@typescript-eslint/typedef,@typescript-eslint/no-var-requires
    const { composeWithDevTools } = require("redux-devtools-extension");
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-return
    return composeWithDevTools(applyMiddleware(...middleware));
  }
  return applyMiddleware(...middleware);
};

export interface ApplicationStore {
  movies: MoviesStore;
}

export const makeStore: MakeStore<Store<ApplicationStore>> = () => {
  const sagaMiddleware: SagaMiddleware = createSagaMiddleware();

  const store: Store<ApplicationStore> = createStore(
    combineReducers<ApplicationStore>({
      movies: moviesReducer
    }),
    bindMiddleware([sagaMiddleware])
  ) as unknown as Store<ApplicationStore>;

  store.sagaTask = sagaMiddleware.run(rootSaga);

  return store;
};

// eslint-disable-next-line @typescript-eslint/typedef
export const wrapper = createWrapper<Store<ApplicationStore>>(makeStore, { debug: true });
