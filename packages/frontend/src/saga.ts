import { Effect, fork } from "redux-saga/effects";
import { all } from "typed-redux-saga";

import { movieSaga } from "~/components/Movie/store/saga";

export default function* rootSaga(): Generator<Effect | Generator, void, void> {
  yield all([fork(movieSaga)]);
}
