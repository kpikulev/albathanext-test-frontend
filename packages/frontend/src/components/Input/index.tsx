import React, { Dispatch, SetStateAction, useMemo, useState } from "react";
import classNames from "classnames";

import styles from "./styles.module.scss";

interface InputProps {
  type?: string;
  className?: string;
  inputClassName?: string;
  iconClassName?: string;
  onChange?: (value: string) => void;
  onFocus?: () => void;
  onBlur?: () => void;
  topPlaceholder?: string;
  placeholder?: string;
  value?: string;
  defaultValue?: string;
  errorText?: string | JSX.Element;
  disabled?: boolean;
  icon?: JSX.Element;
  onClick?: () => void;
  onIconClick?: () => void;
}

export const Input: React.FC<InputProps> = (props: InputProps) => {
  const {
    type,
    className,
    inputClassName,
    iconClassName,
    onChange,
    onFocus,
    onBlur,
    topPlaceholder,
    placeholder,
    value,
    defaultValue,
    errorText,
    disabled,
    icon,
    onClick,
    onIconClick
  }: InputProps = props;

  const [isFocused, setIsFocused]: [boolean, Dispatch<SetStateAction<boolean>>] = useState(() => false);
  const [currentType, setCurrentType]: [string, Dispatch<SetStateAction<string>>] = useState(type ? type : "text");
  const [isErrorState, setIsErrorState]: [boolean, Dispatch<SetStateAction<boolean>>] = useState(!!errorText);

  useMemo(() => {
    setCurrentType(type ?? "");
    setIsFocused(false);
    setIsErrorState(!!errorText);
  }, [type, errorText]);

  const getIcon: () => JSX.Element | undefined = () => {
    if (icon) {
      return (
        <div className={classNames(styles.inputIcon, { [iconClassName ?? ""]: !!iconClassName })} onClick={onIconClick}>
          {icon}
        </div>
      );
    }
  };

  const getTopPlaceholder: () => JSX.Element | boolean | string = () => {
    if (topPlaceholder) {
      return (
        <div
          className={classNames(styles.inputPlaceholder, styles.inputPlaceholderStyleTopPlaceholder, {
            [styles.inputErrorText]: isErrorState
          })}
        >
          {topPlaceholder}
        </div>
      );
    }

    return (
      (value || isFocused) && (
        <div className={classNames(styles.inputPlaceholder, { [styles.inputErrorText]: isErrorState })}>
          {placeholder}
        </div>
      )
    );
  };

  return (
    <div className={classNames(styles.input, { [className ?? ""]: !!className })}>
      {getTopPlaceholder()}

      <input
        type={currentType}
        onChange={(e): void => {
          setIsErrorState(false);

          if (typeof onChange === "function") {
            onChange(e.currentTarget.value);
          }
        }}
        onFocus={(): void => {
          setIsFocused(true);

          if (typeof onFocus === "function") {
            onFocus();
          }
        }}
        onBlur={(): void => {
          setIsFocused(false);

          if (typeof onBlur === "function") {
            onBlur();
          }
        }}
        onClick={onClick}
        placeholder={isFocused ? "" : placeholder}
        value={value}
        defaultValue={defaultValue}
        className={classNames(styles.inputInput, {
          [styles.inputInputStyleError]: isErrorState,
          [styles.inputInputWithIcon]: icon,
          [inputClassName ?? ""]: !!inputClassName
        })}
        disabled={disabled}
      />

      {getIcon()}

      {isErrorState && <div className={classNames(styles.inputErrorText)}>{errorText}</div>}
    </div>
  );
};
