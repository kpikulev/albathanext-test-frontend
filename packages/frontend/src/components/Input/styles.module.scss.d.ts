declare namespace StylesModuleScssNamespace {
  export interface IStylesModuleScss {
    input: string;
    inputErrorText: string;
    inputIcon: string;
    inputInput: string;
    inputInputStyleError: string;
    inputInputWithIcon: string;
    inputPlaceholder: string;
    inputPlaceholderStyleTopPlaceholder: string;
  }
}

declare const StylesModuleScssModule: StylesModuleScssNamespace.IStylesModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: StylesModuleScssNamespace.IStylesModuleScss;
};

export = StylesModuleScssModule;
