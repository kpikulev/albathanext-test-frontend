import * as React from "react";
import classNames from "classnames";
import { ChangeEvent } from "react";

import styles from "./styles.module.scss";

interface IProps {
  className?: string;
  defaultValue?: string | string[];
  value?: string | number | string[];
  sign?: string;
  placeholder?: string;
  onChange?: (e: ChangeEvent<HTMLTextAreaElement>) => void;
  errorText?: string | JSX.Element;
}

export class TextArea extends React.Component<IProps> {
  render(): JSX.Element {
    return (
      <div className={classNames(styles.apTextarea, { [this.props?.className ?? ""]: !!this.props.className })}>
        <textarea
          value={this.props.value}
          defaultValue={this.props.defaultValue}
          placeholder={this.props.placeholder}
          onChange={this.props.onChange}
          className={styles.apTextareaTextarea}
        />

        {this.getErrorText()}
      </div>
    );
  }

  private getErrorText(): JSX.Element | undefined {
    if (this.props.errorText) {
      return <div className={styles.apTextareaErrorText}>{this.props.errorText}</div>;
    }
  }
}
