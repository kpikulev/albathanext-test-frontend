import React from "react";
import { createSelector, Selector } from "reselect";
import { useSelector } from "react-redux";
import { Movie } from "@template/shared/movie/movie.api";
import Image from "next/image";

import styles from "./styles.module.scss";

import { ApplicationStore } from "~/store";

type SelectData = Pick<ApplicationStore, "movies">;
const dataSelector: Selector<ApplicationStore, SelectData> = createSelector(
  (state: ApplicationStore) => state.movies,
  movies => ({ movies })
);

export const MovieComponent: React.FC = () => {
  const selectedData: SelectData = useSelector<ApplicationStore, SelectData>(dataSelector);
  const { movies }: SelectData = selectedData;
  const movie: Movie | null = movies.movie;

  if (!movie) {
    return <></>;
  }

  return (
    <div className={styles.movie}>
      <div className={styles.movieContainer}>
        <div className={styles.movieContainerPoster}>
          <Image alt={movie.original_title} src={movie.poster_path} layout="fill" />
        </div>
        <div className={styles.movieContainerAbout}>
          <div className={styles.movieContainerAboutHeading}>
            <div className={styles.movieContainerAboutHeadingTitle}>{movie.original_title}</div>
            <div className={styles.movieContainerAboutHeadingTagline}>{movie.tagline}</div>
          </div>
          <div className={styles.movieContainerAboutParam}>Budget: {movie.budget}</div>
          <div className={styles.movieContainerAboutParam}>Revenue: {movie.revenue}</div>
          <div className={styles.movieContainerAboutParam}>Release date: {movie.release_date}</div>
          <div className={styles.movieContainerAboutParam}>Vote average: {movie.vote_average}</div>
          <div className={styles.movieContainerAboutParam}>
            Homepage:{" "}
            <a href={movie.homepage} target="_blank" rel="noreferrer">
              {movie.homepage}
            </a>
          </div>
        </div>
      </div>
      <div className={styles.movieOverview}>
        <div className={styles.movieOverviewHeading}>Overview</div>
        <div className={styles.movieOverviewText}>{movie.overview}</div>
      </div>
    </div>
  );
};
