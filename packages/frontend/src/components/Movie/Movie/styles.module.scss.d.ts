declare namespace StylesModuleScssNamespace {
  export interface IStylesModuleScss {
    movie: string;
    movieContainer: string;
    movieContainerAbout: string;
    movieContainerAboutHeading: string;
    movieContainerAboutHeadingTagline: string;
    movieContainerAboutHeadingTitle: string;
    movieContainerAboutParam: string;
    movieContainerPoster: string;
    movieOverview: string;
    movieOverviewHeading: string;
    movieOverviewText: string;
  }
}

declare const StylesModuleScssModule: StylesModuleScssNamespace.IStylesModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: StylesModuleScssNamespace.IStylesModuleScss;
};

export = StylesModuleScssModule;
