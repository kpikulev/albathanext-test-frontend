import { call } from "typed-redux-saga";
import { Effect, put, takeLatest } from "redux-saga/effects";
import { MoviesResponse, MovieResponse } from "@template/shared/movie/movie.api";
import { FetchMovieDto } from "@template/shared/movie/dto/fetch-movie-dto";
import { PayloadAction } from "typesafe-actions";

import { movieService } from "~/components/Movie/service/movie.service";
import { fetchMovie, fetchMovies, setMovie, setMovies } from "~/components/Movie/store/actions";

function* fetchMoviesSaga(): Generator {
  try {
    const moviesResponse: MoviesResponse = yield* call(() => movieService.getMovies());
    yield put(setMovies(moviesResponse.results));
  } catch (err) {
    const error: Error = err as unknown as Error;
    console.error(error);
  }
}

function* fetchMovieSaga(action: PayloadAction<"movies/fetchMovie", FetchMovieDto>): Generator {
  try {
    const movieResponse: MovieResponse = yield* call(() => movieService.getMovie(action.payload.movieId));
    yield put(setMovie(movieResponse.movie));
  } catch (err) {
    const error: Error = err as unknown as Error;
    console.error(error);
  }
}

export function* movieSaga(): Generator<Effect | Generator, void, void> {
  yield takeLatest(fetchMovies, fetchMoviesSaga);
  yield takeLatest(fetchMovie, fetchMovieSaga);
}
