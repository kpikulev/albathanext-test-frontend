import { ActionCreatorBuilder, createAction } from "typesafe-actions";
import { Movie } from "@template/shared/movie/movie.api";
import { HYDRATE } from "next-redux-wrapper";
import { FetchMovieDto } from "@template/shared/movie/dto/fetch-movie-dto";

import { ApplicationStore } from "~/store";

export const fetchMovies: ActionCreatorBuilder<"movies/fetchMovies"> = createAction("movies/fetchMovies")();

export const fetchMovie: ActionCreatorBuilder<"movies/fetchMovie", FetchMovieDto> =
  createAction("movies/fetchMovie")<FetchMovieDto>();

export const setMovies: ActionCreatorBuilder<"movies/setMovies", Movie[]> = createAction("movies/setMovies")<Movie[]>();

export const setMovie: ActionCreatorBuilder<"movies/setMovie", Movie> = createAction("movies/setMovie")<Movie>();

export const moviesHydrate: ActionCreatorBuilder<string, ApplicationStore> = createAction(HYDRATE)<ApplicationStore>();
