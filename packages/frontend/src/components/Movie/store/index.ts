import { Action, combineReducers, Reducer } from "redux";
import { createReducer, PayloadAction } from "typesafe-actions";
import { Movie } from "@template/shared/movie/movie.api";

import { moviesHydrate, setMovie, setMovies } from "~/components/Movie/store/actions";
import { ApplicationStore } from "~/store";

export interface MoviesStore {
  moviesArray: Movie[];
  movie: Movie | null;
}

export const moviesReducer: Reducer<MoviesStore> = combineReducers({
  moviesArray: createReducer<MoviesStore["moviesArray"], Action>([])
    .handleAction(setMovies, (_: MoviesStore["moviesArray"], { payload }: PayloadAction<string, Movie[]>) => {
      return [...payload];
    })
    .handleAction(
      moviesHydrate,
      (_: MoviesStore["moviesArray"], { payload }: PayloadAction<string, ApplicationStore>) => {
        return payload.movies.moviesArray;
      }
    ),
  movie: createReducer<MoviesStore["movie"], Action>(null)
    .handleAction(setMovie, (_: MoviesStore["movie"], { payload }: PayloadAction<string, Movie>) => {
      return payload;
    })
    .handleAction(moviesHydrate, (_: MoviesStore["movie"], { payload }: PayloadAction<string, ApplicationStore>) => {
      return payload.movies.movie;
    })
});
