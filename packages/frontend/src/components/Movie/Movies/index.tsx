import React from "react";
import { createSelector, Selector } from "reselect";
import { useSelector } from "react-redux";
import Link from "next/link";
import Image from "next/image";

import styles from "./styles.module.scss";

import { ApplicationStore } from "~/store";

type SelectData = Pick<ApplicationStore, "movies">;
const dataSelector: Selector<ApplicationStore, SelectData> = createSelector(
  (state: ApplicationStore) => state.movies,
  movies => ({ movies })
);

export const Movies: React.FC = () => {
  const selectedData: SelectData = useSelector<ApplicationStore, SelectData>(dataSelector);
  const { movies }: SelectData = selectedData;

  return (
    <div className={styles.moviesContainer}>
      {movies.moviesArray.map(movie => {
        return (
          <div className={styles.moviesContainerItem} key={movie.id}>
            <Link href={"/movie/" + String(movie.id)} passHref>
              <div className={styles.moviesContainerItemInner} key={movie.id}>
                <div className={styles.moviesContainerItemInnerTitle} key={movie.id}>
                  {movie.original_title}
                </div>
                <Image alt={movie.original_title} src={movie.poster_path} width={300} height={300} layout="fill" />
              </div>
            </Link>
          </div>
        );
      })}
    </div>
  );
};
