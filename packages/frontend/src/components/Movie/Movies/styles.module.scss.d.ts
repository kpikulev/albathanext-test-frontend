declare namespace StylesModuleScssNamespace {
  export interface IStylesModuleScss {
    moviesContainer: string;
    moviesContainerItem: string;
    moviesContainerItemInner: string;
    moviesContainerItemInnerTitle: string;
  }
}

declare const StylesModuleScssModule: StylesModuleScssNamespace.IStylesModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: StylesModuleScssNamespace.IStylesModuleScss;
};

export = StylesModuleScssModule;
