import { MOVIE_ROUTES, MovieFrontendAPI, MoviesResponse, MovieResponse } from "@template/shared/movie/movie.api";

import { apiFetch } from "~/utils/api-fetch";

class MovieService implements MovieFrontendAPI {
  getMovie(id: number): Promise<MovieResponse> {
    return apiFetch(MOVIE_ROUTES.getMovie(String(id)));
  }

  getMovies(this: void): Promise<MoviesResponse> {
    return apiFetch(MOVIE_ROUTES.getMovies());
  }
}

export const movieService: MovieService = new MovieService();
