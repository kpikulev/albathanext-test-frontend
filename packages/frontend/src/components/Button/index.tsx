import * as React from "react";

import styles from "./styles.module.scss";

import { ButtonStyle } from "~/components/Button/enums/ButtonStyle";

interface IProps {
  text: string;
  buttonStyle?: ButtonStyle;
  className?: string;
  href?: string;
  onClick?: (e: React.MouseEvent) => void;
  disabled?: boolean;
}

export class Button extends React.Component<IProps> {
  render(): JSX.Element {
    if (this.props.href) {
      return (
        <a
          className={this.getClassName()}
          href={this.props.href}
          onClick={(e): void => {
            !this.props.disabled && this.props.onClick && this.props.onClick(e);
          }}
        >
          {this.props.text}
        </a>
      );
    } else {
      return (
        <div
          className={this.getClassName()}
          onClick={(e): void => {
            !this.props.disabled && this.props.onClick && this.props.onClick(e);
          }}
        >
          {this.props.text}
        </div>
      );
    }
  }

  private getClassName(): string {
    let className: string = styles.apButton;

    if (this.props.className) {
      className += " " + this.props.className;
    }

    if (this.props.disabled) {
      className += " " + styles.apButtonStyleDisabled;
    }

    const buttonStyleClass: string | null = this.getButtonStyleClass();
    if (buttonStyleClass) {
      className += " " + buttonStyleClass;
    }

    return className;
  }

  private getButtonStyleClass(): string | null {
    const buttonStyle: ButtonStyle = this.props.buttonStyle ?? ButtonStyle.YELLOW;

    let buttonStyleClass: string | null = null;

    if (buttonStyle == ButtonStyle.WHITE) {
      buttonStyleClass = styles.apButtonStyleWhite;
    }

    return buttonStyleClass;
  }
}
