import React from "react";

import styles from "./styles.module.scss";

export const HeaderLogo: React.FC = () => {
  return <div className={styles.headerLogo}>Albathanext test app</div>;
};
