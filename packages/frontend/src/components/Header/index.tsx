import React from "react";

import styles from "./styles.module.scss";

import { HeaderMenu } from "~/components/Header/HeaderMenu";
import { HeaderLogo } from "~/components/Header/HeaderLogo";

export const Header: React.FC = () => {
  return (
    <div className={styles.headerContainer}>
      <HeaderLogo />
      <HeaderMenu />
    </div>
  );
};
