import React from "react";
import Link from "next/link";

import styles from "./styles.module.scss";

export const HeaderMenu: React.FC = () => {
  return (
    <div className={styles.headerMenu}>
      <div className={styles.headerMenuItem}>
        <Link href="/">Main</Link>
      </div>
      <div className={styles.headerMenuItem}>
        <Link href="/movies">Movies</Link>
      </div>
    </div>
  );
};
