import React from "react";
import classNames from "classnames";

import styles from "./styles.module.scss";

interface BlockProps {
  className?: string;
  children?: JSX.Element | JSX.Element[];
}

export const Block: React.FC<BlockProps> = ({ children, className }: BlockProps) => {
  return <div className={classNames(styles.block, { [className ?? ""]: !!className })}>{children}</div>;
};
