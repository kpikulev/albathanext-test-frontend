import React from "react";

import styles from "./styles.module.scss";

interface PageProps {
  children?: JSX.Element | JSX.Element[];
}

export const Page: React.FC<PageProps> = ({ children }: PageProps) => {
  return <div className={styles.pageContainer}>{children}</div>;
};
